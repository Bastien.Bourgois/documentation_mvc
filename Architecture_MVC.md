## La base 

Pour régler les soucis d'organisation d'un projet, il est fortement conseillé d'utiliser un design pattern.

Le design pattern que nous allons voir est le MVC (Model-View-Controller)

Le pattern MVC est une norme pour organiser son code source en vous indiquant que faire dans la création de fichier. 
La logique est séparé en 3 parties que l'on retrouve dans des fichiers distincts:

- Le modèle gère les données du site. Il va aller récupérer les informations dans la base de données, de les organiser et de les assembler pour qu'elles puissent être traitées par le contrôleur (Que l'on voit juste après). On y trouve donc les requêtes SQL.

- La vue gère les affichages. On y trouve surtout du HTML/CSS, ainsi que de petites boucles PHP simples.

- Le contrôleur gère la logique du code et prend des décisions. Il fait l'intermédiaire entre le modèle et la vue. Le contrôleur va demander au modèle les données, les analyser, prendre des décisions et renvoyer de quoi afficher à la vue. Il contient exclusivement du PHP. Il peut aussi gérer les droits d'accés.


![alt tag](images/schéma-encore-mieux.png)

## Les frameworks MVC

Le mieux est de créer son site en se basant sur un framework PHP qui vous demandera du temps pour apprendre à les connaitre.

Voici quelques exemples de frameworks PHP qui se basent sur l'architecture MVC:

- CodeIgniter
- CakePHP
- Symfony
- Jelix
- Zend Framework

## Pour finir 

Utiliser l'architecture MVC pour se repérer et modifier plus facilement son code source 




